<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal("shaun");
echo "Name of Animal is: $sheep->name <br>";
echo "Total legs are: $sheep->legs <br>";
echo "Cold Blooded is: $sheep->cold_blooded <br><br>";

$frog = new frog("buduk");
echo "Name of Animal is: $frog->name <br>";
echo "Total legs are: $frog->legs <br>";
echo "Cold Blooded is: $frog->cold_blooded <br>";
echo  $frog->jump() . "<br><br>";

$ape = new ape("kera sakti");
echo "Name of Animal is: $ape->name <br>";
echo "Total legs are: $ape->legs <br>";
echo "Cold Blooded is: $ape->cold_blooded <br>";
echo  $ape->yell();

?>
